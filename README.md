# UnityGamesLab - The Undead Forest


## Project description

This is a toy project made while learning Unity. Dark fantasy scenery, wandering through the woods, killing some zombie-skeletons with magical spells, visiting abandoned cemeteries, and using magical teleports. 


## Video demo

[Watch on YouTube](https://youtu.be/dBbhOZh3fDA)

## Asset Store packages

Asset Store packages are not included in this repo to save upload/download space. All necessary packages are listed in UndeadForest\Assets\AssetStore_PackagesToDownload.txt. All packages used are free to download from Unity Asset Store.

## Unity Version

Made with Unity 2022.3.8
