using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    [SerializeField] Slider healthSlider;
    [SerializeField] int maxHealth = 100;

    int healthRestorePoints = 10;
    float healthRestorePeriod = 5.0f;
    float nextRestoreTime = 5.0f;
    // Start is called before the first frame update
    void Start()
    {
        healthSlider.maxValue = maxHealth;
        healthSlider.value = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextRestoreTime)
        {
            // points above maxHealth will be clipped by healthSlider under the hood
            healthSlider.value += healthRestorePoints;
            nextRestoreTime = Time.time + healthRestorePeriod;
        }
    }

    public void TakeDamage(int damagePoints)
    {
        healthSlider.value -= damagePoints;
    }
}
