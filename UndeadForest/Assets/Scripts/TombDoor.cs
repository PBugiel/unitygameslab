using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TombDoor : MonoBehaviour, IDamageable
{
    [SerializeField] int pointsToDestruct = 30;

    public void Damage(int damagePoints)
    {
        pointsToDestruct -= damagePoints;
        if (pointsToDestruct <= 0)
            Destroy(gameObject);
    }
}
