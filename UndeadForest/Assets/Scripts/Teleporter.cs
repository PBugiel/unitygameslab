using System;
using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using Unity.VisualScripting;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    [SerializeField] Teleporter targetTeleporter;
    [SerializeField] ParticleSystem teleportationVfx;
    [SerializeField] float fpsControllerDelay = 0.5f;

    //public bool IsTeleporting { get; set; }
    public Transform Location => transform;
    public GameObject ParentGameObject => gameObject;
    public List<GameObject> teleportedObjects = new();

    void OnTriggerEnter(Collider other)
    {
        ReviewTeleportedObjects();
        if (teleportedObjects.Count > 0)
        {
            Debug.Log($"[Teleport] Hello {other.gameObject.name}, {name} is busy!");
        }
        else
        {
            if (targetTeleporter != null)
            {
                    Debug.Log($"[TeleportSender] Hello {other.gameObject.name} ({other.gameObject.transform.position})! {name} is teleporting you to {targetTeleporter.ParentGameObject.name} ({targetTeleporter.Location.position}).");
                    targetTeleporter.teleportedObjects.Add(other.gameObject);
                    teleportedObjects.Add(other.gameObject);
                    Teleport(other.gameObject);
                    Debug.Log($"{other.gameObject.name} current location: {other.gameObject.transform.position}");
            }
            else Debug.LogWarning($"{name} is a dysfunctional teleport (no target teleport)"); // TODO delete
        }
    }

    private void ReviewTeleportedObjects()
    {
        teleportedObjects.RemoveAll(x => x == null);
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log($"[Teleport] Done! {name} finished teleporting {other.gameObject.name}."); // TODO delete
        teleportedObjects.Remove(other.gameObject);
    }

    public void Teleport(GameObject teleportedObject)
    {
        FirstPersonController fpsController = teleportedObject.GetComponent<FirstPersonController>();
        if (fpsController)
        {
            fpsController.enabled = false;
        }
        float newYPosition = CalculateTargetYPosition(teleportedObject);
        teleportedObject.transform.position = new Vector3(
            targetTeleporter.Location.position.x,
            newYPosition,
            targetTeleporter.Location.position.z);
        teleportedObject.transform.localRotation = targetTeleporter.Location.localRotation;

        if (fpsController)
        {
            StartCoroutine(EnableFpsController(fpsController, fpsControllerDelay));
        }
    }

    private float CalculateTargetYPosition(GameObject teleportedObject)
    {
        RaycastHit newGroundHit;
        float newYPosition = targetTeleporter.Location.position.y;
        // find point on the ground underneath the target teleport
        if (Physics.Raycast(targetTeleporter.Location.position, Vector3.down, out newGroundHit))
        {
            RaycastHit currentGroundHit;
            // find current offset from the ground and add it to the ground point of the target position
            if (Physics.Raycast(teleportedObject.transform.position, Vector3.down, out currentGroundHit))
                newYPosition = newGroundHit.point.y + currentGroundHit.distance;
        }

        return newYPosition;
    }

    IEnumerator EnableFpsController(FirstPersonController controller, float delayTime)
    {
        // TODO re-enable the script in OnCollisionExit maybe?
        yield return new WaitForSeconds(delayTime);
        controller.enabled = true;
    }
}
