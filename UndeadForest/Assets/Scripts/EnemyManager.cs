using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] GameObject enemy;
    [SerializeField] Transform[] spawnPoints;
    [SerializeField] float spawnTime = 3.0f;
    [SerializeField] int maxEnemyCount = 20;

    void Start()
    {
        InvokeRepeating("Spawn", 0, spawnTime);
    }

    void Spawn()
    {
        if (CountEnemies() < maxEnemyCount)
        {
            int spawnPointIdx = Random.Range(0, spawnPoints.Length);
            Instantiate(enemy, 
                        spawnPoints[spawnPointIdx].transform.position,
                        spawnPoints[spawnPointIdx].transform.rotation);
        }
    }
    
    int CountEnemies()
    {
        return GameObject.FindGameObjectsWithTag("EnemySkeleton").Length;
    }
}
