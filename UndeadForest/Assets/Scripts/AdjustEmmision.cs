using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustEmmision : MonoBehaviour
{
    Light pointLight;
    Renderer[] renderers;
    float lightIntensity = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        pointLight = gameObject.GetComponentInChildren<Light>();
        renderers = gameObject.GetComponentsInChildren<Renderer>();
        if (pointLight != null && renderers.Length > 0)
        {
            lightIntensity = pointLight.intensity;
            foreach (var renderer in renderers)
                renderer.material.SetFloat("_EmissionPower", lightIntensity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (pointLight != null && renderers.Length > 0 && lightIntensity != pointLight.intensity)
        {
            lightIntensity = pointLight.intensity;
            SetMaterialEmissionTo(lightIntensity);
        }
    }

    public void SetMaterialEmissionTo(float value)
    {
        if (pointLight != null && renderers.Length > 0)
        {
            foreach (var renderer in renderers)
                renderer.material.SetFloat("_EmissionPower", lightIntensity);
        }
    }
}
