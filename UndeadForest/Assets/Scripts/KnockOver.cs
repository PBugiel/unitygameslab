using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Rendering;

public class KnockOver : MonoBehaviour
{
    [SerializeField] GameObject objectToSpawn;
    [SerializeField] Transform spawnLocation;
    [SerializeField] float rockingInterval = 5f;


    Quaternion startRotation;
    bool isKnocked = false;
    PlayableDirector director;

    
    void Start() 
    {
        startRotation = transform.localRotation;
        director = GetComponent<PlayableDirector>();
        StartCoroutine(RockTheGreavestone());
    }

    void Update()
    {
        float xSignedDiff = Quaternion.Angle(startRotation, transform.localRotation);
        if (!isKnocked && Math.Abs(xSignedDiff) > 70f)
        {
            Invoke(nameof(SpawnObjectWhenKnockedOver), 0f);
            isKnocked = true;
            Debug.Log($"Spawning {objectToSpawn.name}!");
        }
    }

    void SpawnObjectWhenKnockedOver()
    {
        GameObject spawnedObject = Instantiate(objectToSpawn, spawnLocation);
        spawnedObject.transform.SetParent(spawnLocation);
        PlayableDirector director = spawnedObject.GetComponent<PlayableDirector>();
        float destroyAfter = 1f;
        if (director) destroyAfter = (float)director.duration;
        Debug.Log($"Destroy after: {destroyAfter}");
        Destroy(spawnedObject, 1.5f);
    }

    IEnumerator RockTheGreavestone()
    {
        while (director && !isKnocked)
        {
            Debug.Log("Playing gravestone rocking animation");
            director.Play();
            yield return new WaitForSeconds(rockingInterval);
        }
        Debug.Log("Cannot play gravestone rocking animation");
    }
}
