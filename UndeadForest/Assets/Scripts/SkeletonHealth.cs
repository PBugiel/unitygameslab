using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonHealth : MonoBehaviour, IDamageable
{
    [SerializeField] int startingHealth = 100;
    [SerializeField] GameObject healthIndicator;

    int health;

    Light healthIndicatorLight;

    void Start()
    {
        health = startingHealth;
        if (healthIndicator != null)
        {
            healthIndicatorLight = GetComponentInChildren<Light>();
        }    
    }

    public void Damage(int damagePoints)
    {
        health -= damagePoints;
        Debug.Log($"Enemy points: {health}");
        if (healthIndicatorLight != null)
            healthIndicatorLight.intensity = (float)health/startingHealth;

        if (health <= 0)
            Destroy(gameObject);
    }
}
