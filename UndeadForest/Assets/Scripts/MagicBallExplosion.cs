using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicBallExplosion : MonoBehaviour
{
    public GameObject explosionObject;
    [SerializeField] int damage = 15;
    public float lifetime = 10.0f;

    private void Awake() 
    {
        Destroy(gameObject, lifetime);
    }

    private void OnCollisionEnter(Collision other) 
    {
        Explode();
        var enemyHealthScript = other.gameObject.GetComponent<IDamageable>();
        if (enemyHealthScript != null)
            enemyHealthScript.Damage(damage);
    }

    void Explode()
    {
        Transform transform = gameObject.transform;
        Destroy(gameObject);
        var explosionParticles = Instantiate(
            explosionObject,
            transform.position,
            transform.rotation
        );
        Destroy(explosionParticles, 2.0f);
    }
}
