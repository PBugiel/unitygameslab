using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingScript : MonoBehaviour
{
    public GameObject flyingSpell;
    [SerializeField] int spellSpeed = 10;

    void Update()
    {
        transform.rotation = transform.parent.rotation;
        if (Input.GetButtonDown("Fire1"))
        {
            ThrowMagic();
        }
    }

    private void ThrowMagic()
    {
        var spellInstance = Instantiate<GameObject>(flyingSpell,
            transform.position, transform.rotation);
        spellInstance.GetComponent<Rigidbody>().AddForce(
            transform.forward*spellSpeed,
            ForceMode.Impulse);
    }
}
