using System.Collections;
using System.Collections.Generic;
using UnityEditor.EditorTools;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Rendering;

public class EnemyAI : MonoBehaviour
{
    // cache
    GameObject player;
    NavMeshAgent agent;
    Animator animator;
    BoxCollider boxCollider;

    // patrol 
    [Header("Patrolling")]
    [Tooltip("Layer containing the terrain on which the Enemy will move.")]
    [SerializeField] LayerMask groundLayer;

    [Tooltip("Range within which a new destination point is chosen when the Enemy is patrolling the level.")]
    [SerializeField] float patrollingRange = 100.0f;
    Vector3 destPoint;
    bool destPointSet;
    float minDistance = 10.0f;

    // chasing and attacking
    [Header("Chasing and attacking")]
    [Tooltip("Layer on which the Player is situated; ideally only the Player should be there.")]
    [SerializeField] LayerMask playerLayer;

    [Tooltip("Range at which the Enemy notices and starts chasingthe Player.")]
    [SerializeField] float sightRange = 10;

    [Tooltip("Range at which the Enemy stops chasing the Player and starts attacking")]
    [SerializeField] float attackRange = 1;

    [Tooltip("Number of points that the enemy takes from the Player in one attack.")]
    [SerializeField] int damagePoints = 7;
    bool playerInSight;
    bool playerInAttackRange;


    void Start()
    {
        // Enemy has to have NavMeshAgent and Animator components set up
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        
        // BoxCollider used for hitting the Player
        // might be a probem if there was more than one BoxCollider on the enemy
        boxCollider = GetComponentInChildren<BoxCollider>();
        player = GameObject.FindWithTag("Player");
    }


    void Update()
    {
        if (agent.isOnNavMesh)
        {
			// Checks if inside a sphere of "sightRange" radius there exists any object on "playerLayer"
			// only the Player is present in "playerLayer"
            playerInSight = Physics.CheckSphere(transform.position, sightRange, playerLayer);
            playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, playerLayer);
            if (!playerInSight && !playerInAttackRange)
                Patrol();
            else if (playerInSight && !playerInAttackRange)
                Chase();
            else
                Attack();
        }
    }


    void Attack()
    {
        //Debug.Log("Attacking");
		
		// enables "Attack" trigger only if attacking animation is not currently playing
        if (!animator.GetCurrentAnimatorStateInfo(0).IsName("SwingQuick"))
        {
            // sets destination to current location - stop walking
            agent.SetDestination(transform.position);
            // sets "Attack" trigger on animator - enables transition between walking and attacking
            animator.SetTrigger("Attack");
        }
    }


    void Chase()
    {
        //Debug.Log("Chasing");
        agent.SetDestination(player.transform.position);
    }


    void Patrol()
    {
        //Debug.Log("Patrolling");
        if (destPointSet)
            agent.SetDestination(destPoint);
        else 
            SearchForPatrolDestination();
        if (Vector3.Distance(transform.position, destPoint) < minDistance)
            destPointSet = false;
    }


    void SearchForPatrolDestination()
    {
        float x = Random.Range(-patrollingRange, patrollingRange);
        float z = Random.Range(-patrollingRange, patrollingRange);
        destPoint = new Vector3(transform.position.x + x, transform.position.y, transform.position.z + z);
        // checking if the destPoint is not outside the map - ground is underneath it
        // TODO enemy can't go uphill more than it's transform.position.y
        if (Physics.Raycast(destPoint, Vector3.down, groundLayer))
        {
            destPointSet = true;
        }
    }

    // invoked in Animation Event
    void EnableAttack()
    {
        boxCollider.enabled = true;
    }

    // invoked in Animation Event
    void DisableAttack()
    {
        boxCollider.enabled = false;
    }

    private void OnTriggerEnter(Collider other) 
    {
        if (other.tag == "Player")
        {
            HitThePlayer(other);
        }
    }

     void HitThePlayer(Collider playerCollider)
    {
        var playerHealth = playerCollider.gameObject.GetComponent<HealthManager>();
        if (playerHealth != null)
            playerHealth.TakeDamage(damagePoints);
        boxCollider.enabled = false;
    }
}
